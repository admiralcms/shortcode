# Installing
Installing this package is as simple as adding it to Composer and telling CakePHP to load it.
```
composer require Admiral/Shortcode
```

```php
// src/Application.php
public function bootstrap() {
  parent::bootstrap();

  $this->addPlugin('Admiral/Shortcode');
}
```

Optionally, one can add the `ShortcodeHelper` to your view.
```php
// src/AppView.php
public function initialize() {
  // ...
  $this->loadHelper('Admiral/Shortcode.Shortcode');

  // ...
}
```