# Creating a shortcode
Shortcodes can be creating by first creating them in your app (or own plugin).
```php
// src/View/Shortcode/ExampleShortcode.php
namespace App\View\Shortcode;

use Admiral\Shortcode\Shortcode;

class ExampleShortcode extends Shortcode {
  protected static function execute(array $params, string $content, string $arg){
    return "hello world!";
  }
}
```

After that is done, you can register it in your bootstrap in order to enable it's usage:
```php
// config/bootstrap.php
Shortcode::registerShortcodes('App', [
 'Example',
]);
```