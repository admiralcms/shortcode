# Parsing Shortcodes
Once you've [installed](installing.md) and [created a shortcode](creating-a-shortcode.md), you might now want to actually start parsing the shortcode from your text.  
This is done simply by calling `Shortcode::doShortcode()`:
```php
// src/Controller/MyController.php
use Admiral\Shortcode\Shortcode;

// ...
$article->body = Shortcode::doShortcode($article->body);
```

Shortcodes will automatically be parsed and replaced with their output.