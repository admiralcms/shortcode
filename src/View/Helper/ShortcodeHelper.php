<?php
namespace Admiral\Shortcode\View\Helper;

use Cake\View\Helper;
use Cake\Core\Configure;
use Cake\View\CellTrait;
use Cake\Event\EventManager;
use Cake\Cache\Cache;

class ShortcodeHelper extends Helper {
  use CellTrait;

  private $shortcodes;

  public function initialize(array $config) {
    $this->shortcodes = Configure::read('shortcodes');
  }

  public function getEventManager() {
    return EventManager::instance();
  }

  /**
  * Shortcodes runner.
  *
  * @param string $content Content to search for shortcodes
  * @return string the output of the shortcodes
  */
  public function doShortcode($content, $opts = []){
    // Check if we have a cached version
    // Return cached version if so
    if(!empty($opts['cache'])) {
      $cache = Cache::read($opts['cache']);
      if($cache) return $cache;
    }

    // Get all the shortcodes
    $shortcodes = $this->get($content);

    // Loop through all shortcodes
    foreach ($shortcodes as $shortcode) {
      // Parse the shortcodes
      $parsed = $this->parse($shortcode);

      // Replace the content with the shortcode output
      $content = str_replace($shortcode['full'], $parsed, $content);
    }

    // Check if we need to cache
    if(!empty($opts['cache'])) Cache::write($opts['cache'], $content);

    // Return the content
    return $content;
  }

  /**
   * Shortcode parser.
   *
   * @param array $shortcode Shortcode to parse
   * @return string
   */
  public function parse(array $shortcode) {
    $result = '';
    if (empty($shortcode)) {
      return $result;
    }
	  if (!array_key_exists(ucfirst($shortcode['name']), $this->shortcodes)){
      return $shortcode['full'];
    }

    $body = $this->cell(ucfirst($shortcode['name']) . 'Shortcode',[
      'params' => $shortcode['params'],
      'content' => $shortcode['content'],
      'args' => $shortcode['args']
    ]);
    $body = str_replace(array("\n", "\r"), '', $body);
    $body = preg_replace('~>\s+<~', '><', $body);

    $content = trim($shortcode['content']);
    $body = preg_replace("/\s*{{\s*the_content\|raw\s*}}/", $content, $body);
    $body = preg_replace("/\s*{{\s*the_content\s*}}/", h($content), $body);

    return $body;
  }

  /**
   * Shortcode regex.
   * Source: https://developer.wordpress.org/reference/functions/get_shortcode_regex/
   *
   * @return string The Regex string
   */
  public function get_shortcode_regex(){
    return
      '\\['                                // Opening bracket
      . '(\\[?)'                           // 1: Optional second opening bracket for escaping shortcodes: [[tag]]
      . "([A-Za-z_]+)"                     // 2: Shortcode name
      . '(?![\\w-])'                       // Not followed by word character or hyphen
      . '('                                // 3: Unroll the loop: Inside the opening shortcode tag
      .     '[^\\]\\/]*'                   // Not a closing bracket or forward slash
      .     '(?:'
      .         '\\/(?!\\])'               // A forward slash not followed by a closing bracket
      .         '[^\\]\\/]*'               // Not a closing bracket or forward slash
      .     ')*?'
      . ')'
      . '(?:'
      .     '(\\/)'                        // 4: Self closing tag ...
      .     '\\]'                          // ... and closing bracket
      . '|'
      .     '\\]'                          // Closing bracket
      .     '(?:'
      .         '('                        // 5: Unroll the loop: Optionally, anything between the opening and closing shortcode tags
      .             '[^\\[]*+'             // Not an opening bracket
      .             '(?:'
      .                 '\\[(?!\\/\\2\\])' // An opening bracket not followed by the closing shortcode tag
      .                 '[^\\[]*+'         // Not an opening bracket
      .             ')*+'
      .         ')'
      .         '\\[\\/\\2\\]'             // Closing shortcode tag
      .     ')?'
      . ')'
      . '(\\]?)'                           // 6: Optional second closing brocket for escaping shortcodes: [[tag]]
      . '\r?\n?\r?\n?';                    // Strip blank lines from the back of the closing tag
  }

  /**
   * Shortcodes getter.
   *
   * @param string $content Content to look for shortcodes
   * @return array
   */
  public function get($content) {
    if (!is_string($content)) {
      return [];
    }
    preg_match_all("/" . $this->get_shortcode_regex() . "/", $content, $matches);
    if (empty($matches[0])) {
      return [];
    }
    $result = [];
    foreach ($matches[0] as $k => $match) {
      $result[] = [
        'full' => $match,
        'name' => $matches[2][$k],
        'args' => preg_replace('/=/', '', $matches[3], 1)[$k],
        'params' => $this->getParams($match),
        'content' => str_replace('<br />', "\r\n", $matches[5][$k])
      ];
    }
    return $result;
  }

  /**
   * Shortcodes stripper.
   * Prevent shortcodes from being shown and/or ran.
   *
   * @param string $content Content to search for shortcodes
   * @return string
   */
  public function stripShortcodes($body) {
    // Get all the shortcodes
    $shortcodes = $this->get($body);

    // Loop through all shortcodes
    // Replace the shortcode with... wait for it... nothing :O
    foreach ($shortcodes as $shortcode) {
      $body = str_replace($shortcode['full'], "", $body);
    }
    
    // Return the content
    return $body;
  }

  /**
   * Get the parameters to pass to a shortcode on execution
   *
   * @param string $shortcode Shortcode
   * @return array
   */
  protected function getParams($shortcode) {
    preg_match_all('/\s(\w+)=([\"\'])(.*?)\2/', $shortcode, $matches);
    if (empty($matches[1])) {
      return [];
    }
    $result = [];
    foreach ($matches[1] as $k => $v) {
      $result[$v] = $matches[3][$k];
    }
    return $result;
  }
}
