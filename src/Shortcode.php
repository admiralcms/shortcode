<?php
namespace Admiral\Shortcode;

use Cake\Core\Configure;
use Cake\View\Cell;

class Shortcode {
  public static function registerShortcodes(string $plugin, array $shortcodes) {
    // Loop over each shortcode specified
    foreach($shortcodes as $shortcode) {
      Self::registerShortcode($plugin, $shortcode);
    }
  }

  public static function registerShortcode(string $plugin, string $shortcode) {
    if(($shortcodes = Configure::read('shortcodes')) != true) {
      $shortcodes = [];
    }

    // Build the namespace we expect the shortcode under
    $ns = '\\' . $plugin . '\\View\\Cell\\' . $shortcode .'ShortcodeCell';

    // Check if the class exists
    if(!class_exists($ns)) throw new \Exception('Class "' . $ns . '" does not exist!');

    // Build our ReflectionClass
    $r = new \ReflectionClass($ns);
    
    // Check if the class is a subclass of this class
    // Also check whether it's not an abstract class
    if(!$r->isSubclassOf(Cell::class)) throw new \Exception('Class "' . $ns . '" does not extend "\Cake\View\Cell"!');
    if($r->isAbstract()) throw new \Exception('Class "' . $ns . '" may not be abstract!');

    // Check if a shortcode with the name already exists
    if(array_key_exists($shortcode, $shortcodes)) throw new \Exception('Shortcode "' . $shortcode . '" is already registered!');
    
    // Create a new instance of the class and register it
    $shortcodes[$shortcode] = $r->newInstance();

    // Save our shortcodes
    Configure::write('shortcodes', $shortcodes);
  }
}
