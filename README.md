# Shortcode

Shortcode executer used for Admiral

## Version Compatibility
Below a table of version compatibility.  
Please note that when a new version is released, support for older versions by the maintainer drop *immediately*.
| Plugin Version | CakePHP Version |
|----------------|-----------------|
| 1.x            | 3.x             |